import React from 'react';

function Search(props) {

    function handleInput(e) {
        props.FilterHandler(e.target.value)
    }
    return (
        <div>
            <input type="text" onChange={handleInput} />
        </div>
    )
}

export default Search;
