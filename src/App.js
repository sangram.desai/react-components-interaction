import React, { Component } from 'react';
import ContactList from "./Components/ContactList";
import Search from './Components/Search'

import axios from "axios";

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      contacts: [],
      filterText: ''
    }
  }

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(response => {
        const newContacts = response.data.map(c => {
          return {
            id: c.id,
            name: c.name
          };
        });

        const newState = Object.assign({}, this.state, {
          contacts: newContacts
        });
        this.setState(newState);
      })
      .catch(error => console.log(error));
  }

  setFilterText(filterString) {
    this.setState({
      filterText: filterString
    })
  }


  render() {
    const filteredContact = this.state.contacts.filter((contact) => {
      return contact.name.toString().toLowerCase().includes(this.state.filterText.toString().toLowerCase())
    });

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">React Contact Manager</h1>
        </header>
        <Search FilterHandler={this.setFilterText.bind(this)} />


        {(this.state.filterText !== '') ? (
          <p>You are searching for '{this.state.filterText}'</p>
        ) : ''}

        <ContactList contacts={filteredContact} />
      </div>
    )
  }
}

export default App



